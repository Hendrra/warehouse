import React from "react";
import {BrowserRouter, Route, Routes} from "react-router-dom";

import "./styles/styles.css"

import Home from "./components/Home";
import Warehouse from "./components/Warehouse";
import Catalogue from "./components/Catalogue";
import Admin from "./components/Admin";
import Create from "./components/Create";
import Update from "./components/Update";
import Delete from "./components/Delete";


const App = () => {

    return(
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/warehouse" element={<Warehouse />} />
                <Route path="/catalogue" element={<Catalogue />} />
                <Route path="/create" element={<Create />} />
                <Route path="/update" element={<Update />} />
                <Route path="/delete" element={<Delete />} />
                <Route path="/admin" element={<Admin />} />
            </Routes>
        </BrowserRouter>
    )
}

export default App;
