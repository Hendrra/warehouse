function validateResponse(response: Response, url: string): Response {
    if (response.ok) {
        console.info("Data fetched correctly from ", url);
    } else {
        console.warn("Error in fetching the data from", url);
    }
    return(response);
}

export default validateResponse;
