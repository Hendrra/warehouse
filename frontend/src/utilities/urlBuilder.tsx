const urlBuilder = (itemID: string, itemName: string, itemPrice: string): string => {
    const searchParams = new URLSearchParams();

    if (itemID !== "") {
        searchParams.append("item_id", itemID);
    }

    if (itemName !== "") {
        searchParams.append("item_name", itemName);
    }

    if (itemPrice !== "") {
        searchParams.append("item_price", itemPrice);
    }

    return(
        "http://localhost:8000/api/items/filter/?" + searchParams.toString()
    );
}

export default urlBuilder;
