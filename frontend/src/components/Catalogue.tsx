import React, {MouseEvent, ReactNode, useState} from "react";

import CSSTextField from "../styles/customStyles";

import urlBuilder from "../utilities/urlBuilder";
import validateResponse from "../utilities/validateResponse";


const Catalogue = () => {

    const [itemID, setItemID] = useState("");
    const [itemName, setItemName] = useState("");
    const [itemPrice, setItemPrice] = useState("");

    const [items, setItems] = useState(
        Array<{id: number, name: string, description: string, price: number, currentDisplay: ReactNode}>
    );

    const handleTextInput = (event: React.ChangeEvent<HTMLInputElement>): void => {
        const value = event.target.value;
        const type_ = event.target.id;

        if (type_ === "textFieldItemID") {
            setItemID(value);
        } else if (type_ === "textFieldItemName") {
            setItemName(value);
        } else {
            setItemPrice(value);
        }
    };

    const displayName = (data: MouseEvent): void => {
        const itemID = data.currentTarget.getAttribute("data-item");
        const newItems = items.map(
            item => item.id === parseInt(itemID as string) ? {...item, currentDisplay: <h2>{ item.name }</h2>} : item
        );
        setItems(newItems);
    };

    const displayDetails = (data: MouseEvent): void => {
        const itemID = data.currentTarget.getAttribute("data-item");
        const newItems = items.map(
            item => item.id === parseInt(itemID as string) ? {
                ...item, currentDisplay:
                    <div>
                        <p>{ item.description } Unique ID: { item.id }</p>
                        <p>Available for { item.price } $</p>
                    </div>
            } : item
        );
        setItems(newItems);
    };

    const handleSubmit = async () => {
        const url = urlBuilder(itemID, itemName, itemPrice);
        await fetch(url, {method: "GET", headers: {Accept: "application/json"}})
            .then((response) => validateResponse(response, url))
            .then((response) => response.clone().json())
            .then((data) => setItems(data.map((item: {id: number, name: string, description: string, price: number}) => (
                {
                    id: item.id,
                    name: item.name,
                    description: item.description,
                    price: item.price,
                    currentDisplay: <h2>{item.name}</h2>,
                }
            ))));
        setItemID("");
        setItemName("");
        setItemPrice("");
    };

    return(
        <div className="container">
            <h1>Your catalogue.</h1>
            <h2>Please choose the item you want to check.</h2>
            <hr />
            <div>
                <CSSTextField
                    id="textFieldItemID" label="Item ID" value={ itemID } onChange={ handleTextInput }
                />
                <CSSTextField
                    id="textFieldItemName" label="Item Name" value={ itemName } onChange={ handleTextInput }
                />
                <CSSTextField
                    id="textFieldItemPrice" label="Price $" value={ itemPrice } onChange={ handleTextInput }
                />
            </div>
            <button className="button" onClick={ handleSubmit }>Submit</button>
            {
                items.length > 0 && (
                    items.map((item: {id: number, name: string, description: string, price: number, currentDisplay: ReactNode}) => (
                        <button
                        className="container-for-single-item"
                        id={ item.id.toString() } key={ item.id } data-item={ item.id }
                        onMouseEnter={ displayDetails }
                        onMouseLeave={ displayName }
                        >
                            { item.currentDisplay }
                        </button>
                    ))
                )
            }
        </div>
    );

}

export default Catalogue;
