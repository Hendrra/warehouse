import React, {useState} from "react";

import CSSTextField from "../styles/customStyles";

import validateResponse from "../utilities/validateResponse";


const url = "http://localhost:8000/create_tables/"


const Admin = () => {

    const [password, setPassword] = useState("");
    const [response, setResponse] = useState(<h2></h2>);

    const handleTextInput = (event: React.ChangeEvent<HTMLInputElement>): void => {
        const newValue = event.target.value;
        setPassword(newValue);
    };

    const handleClick = async (): Promise<void> => {
        try {
            await fetch(url + password, {method: "GET", headers: {Accept: "application/json"}})
            .then((response) => validateResponse(response, url))
            .then((response) => response.clone().json())
            .then((data) => setResponse(<h2>{ data.message }</h2>)
            );
        } catch (error) {
            setResponse(<h2>Cannot connect with the server. Try again later.</h2>);
        }
        setPassword("");
    };

    return(
        <div className="container">
            <h1>Your admin page.</h1>
            <h2>Please fill your password in order to create your tables.</h2>
            <hr />
            <div>
                <CSSTextField
                    id="tfPassword" label="Password" value={ password } onChange={ handleTextInput }
                />
            </div>
            <button className="button" onClick={ handleClick }>Submit</button>
            { response }
        </div>
    );

}

export default Admin;
