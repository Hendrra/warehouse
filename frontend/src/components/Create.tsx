import React, {useState} from "react";

import CSSTextField from "../styles/customStyles";

import validateResponse from "../utilities/validateResponse";


const url = "http://localhost:8000/api/items/"


const Create = () => {

    const [itemName, setItemName] = useState("");
    const [itemDescription, setItemDescription] = useState("");
    const [itemPrice, setItemPrice] = useState("");

    const [response, setResponse] = useState(<div></div>);

    const handleTextInput = (event: React.ChangeEvent<HTMLInputElement>): void => {
        const value = event.target.value;
        const type_ = event.target.id;

        if (type_ === "textFieldName") {
            setItemName(value);
        } else if (type_ === "textFieldDescription") {
            setItemDescription(value);
        } else {
            setItemPrice(value);
        }
    };

    const handleCreate = async () => {
        try {
            await fetch(url, {method: 'POST', headers: {'Content-Type': 'application/json'}, body:
                    JSON.stringify({
                        name: itemName,
                        description: itemDescription,
                        price: itemPrice,
                    }),
            })
                .then((response) => validateResponse(response, url))
                .then((response) => response.clone().json())
                .then((data) => setResponse(
                    <div>
                        <h2>Your item has been added to your warehouse.</h2>
                        <h2>Summary</h2>
                        <h3>{ data.name }. { data.description } Price: { data.price }$. Item ID: { data.id }</h3>
                    </div>
                ));
        } catch (error) {
            setResponse(<h2>Cannot add your item to your warehouse.</h2>);
        }
        setItemName("");
        setItemDescription("");
        setItemPrice("");
    };

    return(
        <div className="container">
            <h1>Create items.</h1>
            <h2>Please fill the template to create your product.</h2>
            <hr />
            <div>
                <CSSTextField
                    id="textFieldName" label="Name" value={ itemName } onChange={ handleTextInput }
                />
                <CSSTextField
                    id="textFieldDescription" label="Description" value={ itemDescription } onChange={ handleTextInput }
                />
                <CSSTextField
                    id="textFieldPrice" label="Price $" value={ itemPrice } onChange={ handleTextInput }
                />
            </div>
            <button className="button" onClick={ handleCreate }>Create</button>
            { response }
        </div>
    );

}

export default Create;
