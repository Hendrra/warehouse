import React from "react";
import {Link} from "react-router-dom";
import {ENV} from "../constants/environment";

const Home = () => {

    console.log("ENV", ENV);

    return(
        <div className="container">
            <h1>Welcome to your warehouse!</h1>
            <h2>Click on the proper button to perform some actions.</h2>
            <hr />
            <div>
                <p>Client</p>
                <Link to={"/warehouse"}>
                    <button className="button">Warehouse</button>
                </Link>

                <Link to={"/catalogue"}>
                    <button className="button">Catalogue</button>
                </Link>
            </div>
            <div>
                <p>Owner</p>
                <Link to={"/create"}>
                    <button className="button">Create</button>
                </Link>

                <Link to={"/update"}>
                    <button className="button">Update</button>
                </Link>

                <Link to={"/delete"}>
                    <button className="button">Delete</button>
                </Link>
            </div>
            <div>
                <p>Administrator</p>
                <Link to={"/admin"}>
                <button className="button">Make Tables</button>
                </Link>
            </div>
        </div>
    );

}

export default Home;
