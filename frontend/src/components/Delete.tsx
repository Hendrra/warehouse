import React from "react";
import {MouseEvent, ReactNode, useEffect, useState} from "react";

import validateResponse from "../utilities/validateResponse";


const url = "http://localhost:8000/api/items/"


const Delete = () => {

    const [items, setItems] = useState(
        Array<{id: number, name: string, description: string, price: number, currentDisplay: ReactNode}>
    );

    const getItems = async () => {
        try {
            return await fetch(url, {method: "GET", headers: {Accept: "application/json"}})
                .then((response) => validateResponse(response, url))
                .then((response) => response.json())
                .then((data) => setItems(data.map((item: {id: number, name: string, description: string, price: number}) => (
                    {
                        id: item.id,
                        name: item.name,
                        description: item.description,
                        price: item.price,
                        currentDisplay: <h2>{ item.name }</h2>,
                    }
                    )
                )));
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        getItems();
    }, []);

    const displayDetails = (data: MouseEvent): void => {
        const itemID = data.currentTarget.getAttribute("data-item");

        const newItems = items.map(
            item => item.id === parseInt(itemID as string) ? { ...item, currentDisplay:
                    <div>
                        <p>{ item.description }</p>
                        <p>Available for { item.price } $</p>
                    </div>
            } : item
        );
        setItems(newItems);
    };

    const displayName = (data: MouseEvent): void => {
        const itemID = data.currentTarget.getAttribute("data-item");

        const newItems = items.map(
            item => item.id === parseInt(itemID as string) ? { ...item, currentDisplay: <h2>{ item.name }</h2> } : item
        );
        setItems(newItems);
    };

    const deleteItem = async (data: MouseEvent) => {
        const itemID = data.currentTarget.getAttribute("data-item");

        try {
            await fetch(url + itemID, {method: "DELETE"})
                .then((response) => validateResponse(response, url + itemID))

            const newItems = items.filter((item) => item.id !== parseInt(itemID as string));
            setItems(newItems);
        } catch (error) {
            console.error(error)
        }
    }

    return(
        <div className="container">
            <h1>Delete items.</h1>
            <h2>Click on the item you wish to delete.</h2>
            <hr />
            {
                items.map((item) => (
                    <button
                        className="container-for-single-item"
                        id={ item.id.toString() } key={ item.id } data-item={ item.id }
                        onMouseEnter={ displayDetails }
                        onMouseLeave={ displayName }
                        onClick={ deleteItem }
                    >
                        { item.currentDisplay }
                  </button>
                ))
            }
        </div>
    );

}

export default Delete;
