import React, {useState} from "react";

import CSSTextField from "../styles/customStyles";

import validateResponse from "../utilities/validateResponse";
import urlBuilder from "../utilities/urlBuilder";


const url = "http://localhost:8000/api/items/"


const Update = () => {

    const [itemID, setItemID] = useState("");
    const [newName, setNewName] = useState("");
    const [newDesc, setNewDesc] = useState("");
    const [newPrice, setNewPrice] = useState("");

    const [item, setItem] = useState({id: "", name: "", description: "", price: ""});
    const [body, setBody] = useState({name: "", description: "", price: 0});
    const [response, setResponse] = useState({error: false, updated: false, message: <h2></h2>});

    const handleTextInput = (event: React.ChangeEvent<HTMLInputElement>): void => {
        const value = event.target.value;
        const type_ = event.target.id;

        if (type_ === "textFieldItemID") {
            setItemID(value);
        } else if (type_ === "textFieldNewName") {
            setNewName(value);
        } else if (type_ === "textFieldNewDesc") {
            setNewDesc(value);
        } else {
            setNewPrice(value);
        }
    };

    const bodyBuilder = (data: Array<{id: string, name: string, description: string, price: string}>): void => {
        if (data.length > 0) {
            const information: {id: string, name: string, description: string, price: string} = data[0];
            setItem(information);

            let name: string;
            let desc: string;
            let price: string;

            if (newName === "") {
                name = information.name
            } else {
                name = newName
            }

            if (newDesc === "") {
                desc = information.description
            } else {
                desc = newDesc
            }

            if (newPrice === "") {
                price = information.price
            } else {
                price = newPrice
            }

            setBody({
                name: name,
                description: desc,
                price: parseFloat(price),
            })
        } else {
            setResponse({
                error: true,
                updated: false,
                message: <h2>Something is wrong. Please check the ID provided.</h2>
            })
        }

    }

    const handleUpdate = async () => {
        const urlGet = urlBuilder(itemID, "", "");
        await fetch(urlGet, {method: "GET", headers: {Accept: "application/json"}})
            .then((response) => validateResponse(response, urlGet))
            .then((response) => response.clone().json())
            .then((data) => bodyBuilder(data));
    }

    const handleProceed = async () => {
        await fetch(url + itemID, {
            method: "PUT", headers: {'Content-Type': 'application/json'}, body: JSON.stringify(body)
        })
            .then((response) => validateResponse(response, url + itemID))
            .then((response) => response.clone().json())
            .then((data) => setResponse({
                error: false,
                updated: true,
                message: <h2>Your item (id={data.id}) has been updated.</h2>
            }));
    }

    return(
        <div className="container">
            <h1>Update items.</h1>
            <h2>Please insert the ID of the item you want to update and the new values.</h2>
            <hr />
            <div>
                <CSSTextField
                    id="textFieldItemID" label="Item ID" onChange={ handleTextInput }
                />
                <CSSTextField
                    id="textFieldNewName" label="New name" onChange={ handleTextInput }
                />
                <CSSTextField
                    id="textFieldNewDesc" label="New description" onChange={ handleTextInput }
                />
                <CSSTextField
                    id="textFieldNewPrice" label="New price $" onChange={ handleTextInput }
                />
            </div>
            <button className="button" onClick={ handleUpdate }>Submit</button>
            {
                item.name !== "" && (
                    <div>
                        <h2>Your changes: </h2>
                        <h3>Name: "{ item.name }" ={"->"} "{ body.name }"</h3>
                        <h3>Description: "{ item.description }" ={"->"} "{ body.description }"</h3>
                        <h3>Price: { item.price }$ ={"->"} { body.price }$</h3>
                        <hr />
                        <h2>If you want to proceed please click the button bellow. Otherwise change your input.</h2>
                        <button className="button" onClick={ handleProceed }>Proceed</button>
                        {
                            response.updated && (
                                <div>{ response.message }</div>
                            )
                        }
                    </div>
                )
            }
            {
                response.error && (
                    <div>{ response.message }</div>
                )
            }
        </div>
    );

}

export default Update;
