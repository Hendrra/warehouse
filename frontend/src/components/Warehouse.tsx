import React from "react";
import {MouseEvent, ReactNode, useEffect, useState} from "react";

import validateResponse from "../utilities/validateResponse";


const url = "http://localhost:8000/api/items/"


const Warehouse = () => {

    const [items, setItems] = useState(
        Array<{id: number, name: string, description: string, price: number, currentDisplay: ReactNode}>
    );

    const getItems = async () => {
        try {
            return await fetch(url, {method: "GET", headers: {Accept: "application/json"}})
                .then((response) => validateResponse(response, url))
                .then((response) => response.json())
                .then((data) => setItems(data.map((item: {id: number, name: string, description: string, price: number}) => (
                    {
                        id: item.id,
                        name: item.name,
                        description: item.description,
                        price: item.price,
                        currentDisplay: <h2>{ item.name }</h2>,
                    }
                    )
                )));
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        getItems();
    }, []);

    const displayDetails = (data: MouseEvent): void => {
        const itemID = data.currentTarget.getAttribute("data-item");

        const newItems = items.map(
            item => item.id === parseInt(itemID as string) ? { ...item, currentDisplay:
                    <div>
                        <p>{ item.description }</p>
                        <p>Available for { item.price } $</p>
                    </div>
            } : item
        );
        setItems(newItems);
    };

    const displayName = (data: MouseEvent): void => {
        const itemID = data.currentTarget.getAttribute("data-item");

        const newItems = items.map(
            item => item.id === parseInt(itemID as string) ? { ...item, currentDisplay: <h2>{ item.name }</h2> } : item
        );
        setItems(newItems);
    };

    return(
          <div className="container">
              <h1>Your warehouse.</h1>
              <h2>All your items are listed here.</h2>
              <hr />
               {
                  items.map((item) => (
                  <button className="container-for-single-item"
                          id={ item.id.toString() } key={ item.id } data-item={ item.id }
                          onMouseEnter={ displayDetails }
                          onMouseLeave={ displayName }
                  >
                      { item.currentDisplay }
                  </button>
                  ))
              }
          </div>
    );

}

export default Warehouse;
