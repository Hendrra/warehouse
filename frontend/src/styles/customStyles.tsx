import {styled, TextField} from "@mui/material";

const CSSTextField = styled(TextField)(
    {
        "& label": {
            color: "#553c9a",
            "&.Mui-focused": {
                color: "#553c9a",
            },
        },
        '& .MuiInputBase-input': {
            color: "#553c9a",
        },
        '& .MuiOutlinedInput-root': {
            margin: "1px",
            '& fieldset': {
                borderImage: "linear-gradient(to right, #553c9a, #ee4b2b, #553c9a) 1",
            },
        },
    });

export default CSSTextField;
