const path = require('path')

const Dotenv = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const HtmlMinimizerPlugin = require("html-minimizer-webpack-plugin");
const TerserPlugin = require('terser-webpack-plugin');


let config = {
  entry: './src/index.tsx',

  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'bundle.js'
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'ts-loader',
        }
      },
      {
        test: /\.css/,
        exclude: /node_modules/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },

  resolve: {
    extensions: ['.tsx', 'ts', '.js'],
  },

  optimization: {
    minimize: true,
    minimizer: [
        new CssMinimizerPlugin(),
    ],
  },

  devServer: {
    static: {
      directory: path.resolve(__dirname, './dist'),
    },
    port: 3000,
  },
};

module.exports = (env, argv) => {

  config.plugins = [
      new HtmlWebpackPlugin({
        template: path.join(__dirname,'/public/index.html')
      }),
      new Dotenv({
        path: `./environments/.env.${ argv.mode }`
      }),
      new MiniCssExtractPlugin(),
      new HtmlMinimizerPlugin(),
      new TerserPlugin(),
   ]

  if (argv.mode === 'development') {
    config.devtool = 'inline-source-map'
  }

  return config;
}