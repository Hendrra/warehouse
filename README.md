# Warehouse

## Purpose
This is my first attempt to build a full stack application with Docker, backend written in Python, a React frontend
application and a postgreSQL database integrated. That's a very simple website, more like a playground or an example.
My main goal was to understand how a web application works and to have something in my portfolio.


Being completely honest, not much of styling is added here, moreover there are no backend or frontend tests which is 
even worse. I promise that I will work on that in my future projects, please stay tuned!

However, I would like to notice that this application supports the most important HTTP methods, i.e. GET, POST, PUT and 
DELETE.

## Using the website
The best way to open this application locally is to use Docker. After it's installed please navigate to the main folder
and paste the following command
```
docker-compose build
```
and after it's done
```
docker-compose up
```
The website should be available under the following address: https://localhost:3000.

## Support
If you have any questions about this code feel free to email me @dawidhanrahan@gmail.com.

## License
[MIT](https://gitlab.com/Hendrra/warehouse/-/blob/main/LICENSE)

## Project status
The project is completed and no further work is planned.
