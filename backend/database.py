import sqlalchemy as sql
import sqlalchemy.ext.declarative as declarative
import sqlalchemy.orm as orm
import os

docker = os.environ.get("DOCKER", False)

if not docker:
    DB_HOST = "localhost"
else:
    DB_HOST = os.getenv('DB_HOST')

DATABASE_URL = f"postgresql://postgres:postgres@{DB_HOST}:5432/test"


engine = sql.create_engine(DATABASE_URL)
session = orm.sessionmaker(autocommit=False, autoflush=False, bind=engine)
base = declarative.declarative_base()
