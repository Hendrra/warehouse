from typing import TYPE_CHECKING, List

from sqlalchemy import and_

from .models import Items
from .schemas import Item, UpdateOrCreateItem

if TYPE_CHECKING:
    from sqlalchemy.orm import Session


async def service_get_all_items(db: "Session") -> List[Item]:
    items = db.query(Items).all()
    return list(map(Item.from_orm, items))


async def service_get_filtered_items(
    item_id: int | None, item_name: str | None, item_price: float | None, db: "Session"
) -> List[Item]:
    arguments = [
        (var, value)
        for var, value in locals().items()
        if "item" in var and value is not None
    ]
    query = db.query(Items)

    for var, value in arguments:
        query = query.filter(and_(getattr(Items, var.split("_")[-1]) == value))

    items = query.all()
    return list(map(Item.from_orm, items))


async def service_create_item(item: UpdateOrCreateItem, db: "Session") -> Item:
    item = Items(**item.dict())

    db.add(item)
    db.commit()
    db.refresh(item)

    return Item.from_orm(item)


async def service_get_item(item_id: int, db: "Session"):
    item = db.query(Items).filter(Items.id == item_id).first()
    return item


async def service_delete_item(item: Items, db: "Session"):
    db.delete(item)
    db.commit()


async def service_update_item(
    item: Items, item_data: UpdateOrCreateItem, db: "Session"
) -> Item:

    item.name = item_data.name
    item.description = item_data.description
    item.price = item_data.price

    db.commit()
    db.refresh(item)

    return Item.from_orm(item)
