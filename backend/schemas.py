from pydantic import BaseModel


class ItemBase(BaseModel):
    name: str
    description: str | None = None
    price: float | None = None


class Item(ItemBase):
    id: int

    class Config:
        orm_mode = True


class UpdateOrCreateItem(ItemBase):
    pass
