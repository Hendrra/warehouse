from .database import base
import sqlalchemy as sql


class Items(base):
    __tablename__ = "items"

    id = sql.Column(sql.Integer, primary_key=True, index=True)
    name = sql.Column(sql.String, nullable=False)
    description = sql.Column(sql.String)
    price = sql.Column(sql.Float)
