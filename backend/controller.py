from .database import session, base, engine


def get_db():
    db = session()
    try:
        yield db
    finally:
        db.close()


def add_tables():
    return base.metadata.create_all(bind=engine)
