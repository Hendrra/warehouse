from typing import TYPE_CHECKING, List

import fastapi
import sqlalchemy.orm as orm
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from .controller import add_tables, get_db
from .schemas import Item, UpdateOrCreateItem
from .services import (
    service_create_item,
    service_delete_item,
    service_get_all_items,
    service_get_filtered_items,
    service_get_item,
    service_update_item,
)

if TYPE_CHECKING:
    from sqlalchemy.orm import Session


app = FastAPI()


origins = [
    "http://localhost:3000",
    "localhost:3000",
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


##################################################
# Services GET
##################################################


@app.get("/")
async def root():
    """
    Returns a message displayed on the root/home page. Does not involve any service.
    :return: dict with a message.
    """
    return {"message": "Welcome to Fast-API project!"}


@app.get("/api/items/", response_model=List[Item])
async def get_all_items(db: orm.Session = fastapi.Depends(get_db)):
    """
    Returns all the items which are currently stored in the database
    :param db: current DB session.
    :return: list containing all the elements from the database.
    """
    return await service_get_all_items(db=db)


@app.get("/api/items/filter/", response_model=List[Item])
async def get_filtered_items(
    item_id: int | None = None,
    item_name: str | None = None,
    item_price: float | None = None,
    db: orm.Session = fastapi.Depends(get_db),
):
    """
    Returns all items that meets the filtering criterion. If none of the parameters
    are filled then it returns all items in the database
    :param item_id: the id of the item,
    :param item_name: the name of the item,
    :param item_price: the price of the item,
    :param db: current DB session.
    :return: list containing the filtered elements from the database.
    """
    if not item_id and not item_name and not item_price:
        return await service_get_all_items(db=db)

    return await service_get_filtered_items(
        item_id=item_id, item_name=item_name, item_price=item_price, db=db
    )


@app.get("/create_tables/{password}/")
async def create_tables(password: str):
    """
    Creates the table. Needed when a new database is set and no tables are created,
    requires a password to do the creation
    :param password: str.
    :return: dict with a message whether the tables were created.
    """
    if password == "1q2w3e4r5t":
        try:
            add_tables()
            return {"message": "Tables have been updated."}
        except Exception as error:
            if "SCRAM" in str(error):
                message = "Please run your containers using virtual devices."
            else:
                message = "There's an issue with creating your tables."
            return {"message": message}
    else:
        return {"message": "Wrong password. You can't update tables."}


##################################################
# Services POST
##################################################


@app.post("/api/items/", response_model=Item)
async def create_item(
    item: UpdateOrCreateItem, db: orm.Session = fastapi.Depends(get_db)
):
    """
    Creates an item, i.e. adds a new entry to a database
    :param item: the values to fill inside a database, the ID of the item is set automatically,
    :param db: current DB Session.
    :return: newly created Item.
    """
    return await service_create_item(item=item, db=db)


##################################################
# Services DELETE
##################################################


@app.delete("/api/items/{item_id}/")
async def delete_item(item_id: int, db: orm.Session = fastapi.Depends(get_db)):
    """
    Deletes an item by its id
    :param item_id: the id of an item to be deleted,
    :param db: current DB Session.
    :return: dict with a message that selected item was deleted.
    """
    item = await service_get_item(item_id=item_id, db=db)
    if item is None:
        raise fastapi.HTTPException(status_code=404, detail="Item does not exist")

    await service_delete_item(item=item, db=db)

    return f"Item (id={item_id}) has been deleted successfully"


##################################################
# Services PUT
##################################################


@app.put("/api/items/{item_id}/", response_model=Item)
async def update_item(
    item_id: int,
    item_data: UpdateOrCreateItem,
    db: orm.Session = fastapi.Depends(get_db),
):
    """
    Updates item by its id
    :param item_id: the id of an item to be updated,
    :param item_data: the new parameters for an item (update dict),
    :param db: current DB Session.
    :return: updated Item.
    """
    item = await service_get_item(item_id=item_id, db=db)
    if item is None:
        raise fastapi.HTTPException(status_code=404, detail="Item does not exist")

    return await service_update_item(item=item, item_data=item_data, db=db)
